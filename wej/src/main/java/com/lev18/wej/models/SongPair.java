package com.lev18.wej.models;

import java.io.Serializable;

/**
 * Created by Jeremy on 2/22/14.
 */
public class SongPair implements Comparable, Serializable {
    public Song songObject;
    public int score;

    @Override
    public int compareTo(Object o) {
        SongPair two = (SongPair)o;
        if (score > two.score) {
            return 1;
        } else if (score == two.score) {
            return 0;
        }
        return -1;
    }

    public SongPair(Song song, int score) {
        this.songObject = song;
        this.score = score;
    }

    public SongPair() {

    }
}
