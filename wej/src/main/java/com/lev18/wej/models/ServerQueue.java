package com.lev18.wej.models;

import com.lev18.wej.Global;
import com.lev18.wej.WeJClient;
import com.lev18.wej.messages.InitializeClientQueueMessage;
import com.lev18.wej.messages.UpdateClientQueueMessage;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Created by Jeremy on 2/22/14.
 */
public class ServerQueue implements Queue {

    final private ConcurrentHashMap<String, HashMap<String, Integer>> voteData;  // Map of TrackID -> (Map of ClientID -> score)
    private String currentSongID;
    final private Set<String> clientIpList;
    final private ArrayList<SongPair> list;  // This should never be accessed directly as long as listIsDirty is around
    private Boolean listIsDirty;  // TODO: is this needed?

    public ServerQueue() {
        this.voteData = new ConcurrentHashMap<String, HashMap<String, Integer>>();
        this.currentSongID = null;
        this.clientIpList = new HashSet<String>();
        this.list = new ArrayList<SongPair>();
        this.listIsDirty = true;
    }

    /**
     * Builds the sorted list of SongPairs
     */
    private synchronized void rebuildList() {
        // Empty out the list
        this.list.clear();

        SongPair currentSong = null;

        // Get all track's score map
        for (String songID : this.voteData.keySet()) {
            SongPair voteCount = this.computeSongPair(songID);

            if (songID.equals(this.currentSongID)) {
                // Save the currently playing song so we can push it to the front of the list later
                currentSong = voteCount;
            } else {
                // Normal case, just add the song to the list
                this.list.add(voteCount);
            }
        }

        // Sort and reverse it
        Collections.sort(this.list);
        Collections.reverse(this.list);

        // If there is a currently playing song, add it to the front of the list
        if (currentSong != null) {
            this.list.add(0, currentSong);
        }

        // Mark the list as being clean
        this.listIsDirty = false;
    }

    /**
     * Sums all of the votes for a certain song and returns a new SongPair object
     * @param songID a unique identifier for the song
     * @return a newly computed SongPair
     */
    private SongPair computeSongPair(String songID) {
        HashMap<String, Integer> clientToScoreMap = this.voteData.get(songID);

        // Compute the sum
        int score = 0;
        for (String clientID : clientToScoreMap.keySet()) {
            score += clientToScoreMap.get(clientID);
        }
        SongPair voteCount = new SongPair();
        voteCount.score = score;

        // query to musicDB to get the song object based on songID
        voteCount.songObject = Global.getInstance().musicDB.getSongByID(songID);
        return voteCount;
    }

    /**
     * Registers a vote on a track for a certain client
     * @param clientID a unique identifier for the voting user
     * @param trackID a unique identifier for the track
     * @param score a vote score of -1, 0, or 1
     */
    @Override
    public void vote(final String clientID, final String trackID, final int score) {
        final ServerQueue thisServerQueue = this;
        new Thread(new Runnable() {
            @Override
            public void run() {
                final boolean startedEmpty = (thisServerQueue.voteData.size() == 0);

                HashMap<String, Integer> currentVoterMap;

                // The current votes database does not contain the track, so make a new vote map
                currentVoterMap = thisServerQueue.voteData.putIfAbsent(trackID, new HashMap<String, Integer>());
                if (currentVoterMap == null) {
                    currentVoterMap = thisServerQueue.voteData.get(trackID);
                }
                currentVoterMap.put(clientID, score);

                // Mark the list as being dirty
                thisServerQueue.listIsDirty = true;

                // If the queue was empty at first, play whatever got added to the queue immediately
                if (startedEmpty) {
                    Global.getInstance().getPlayer().play(getFront());
                }

                // Skip/remove song if the score falls below 0
                if (thisServerQueue.computeSongPair(trackID).score < 0) {
                    if (Global.getInstance().getQueue().getFront().id.equals(trackID)) {
                        // Currently playing
                        Global.getInstance().getPlayer().skipTrack();
                    }
                    else {
                        thisServerQueue.voteData.remove(trackID);
                        thisServerQueue.listIsDirty = true;
                    }
                }

                // Possibly rebuild the list with the new vote
                thisServerQueue.getList();

                // Update local UI
                Global.getInstance().updateQueueList();

                // Update the clients that a vote has been made
                thisServerQueue.updateClients();

            }
        }).start();
    }

    /**
     * Gets the current queue
     * @return the queue
     */
    @Override
    public ArrayList<SongPair> getList() {
        // List is dirty, replace its contents and return it
        if (this.listIsDirty) {
            this.rebuildList();
            return this.list;
        }

        // List is clean, can just return it
        return this.list;
    }

    /**
     * Gets the front of the queue
     * @return the front Song of the queue, or null if queue is empty
     */
    @Override
    public Song getFront() {
        ArrayList<SongPair> voteList = this.getList();

        if (voteList.size() == 0) {
            return null;
        }
        SongPair newCurrentSong = voteList.get(0);
        this.currentSongID = newCurrentSong.songObject.id;
        return newCurrentSong.songObject;
    }

    /**
     * Remove the front item from the queue
     */
    @Override
    public void dequeue() {
        // Remove the currently playing track
        this.voteData.remove(currentSongID);

        // Get the new list
        this.rebuildList();
        ArrayList<SongPair> voteList = this.getList();

        // Nothing left, just return
        if (voteList.size() < 1) {
            this.currentSongID = null;
            return;
        }

        // Set the next current song
        SongPair newCurrentSong = voteList.get(0);
        this.currentSongID = newCurrentSong.songObject.id;

        // Rebuild the new list
        this.rebuildList();

        this.updateClients();
    }

    @Override
    public void addClient(final String clientIP) {
        this.clientIpList.add(clientIP);

        // Initialize the client
        final ArrayList<SongPair> voteList = this.getList();
        final MusicDB musicDB = Global.getInstance().musicDB;
        final String myIP = Global.getInstance().getMyServerInfo().getIpaddr();

        WeJClient cl = new WeJClient();
        cl.send(clientIP, new InitializeClientQueueMessage(
                myIP,
                clientIP,
                musicDB,
                voteList
        ));
    }

    public void updateClients() {
        final ArrayList<SongPair> list = this.getList();
        final Set<String> clist = this.clientIpList;
        new Thread() {
            public void run() {
                final String myIP = Global.getInstance().getMyServerInfo().getIpaddr();

                WeJClient cl = new WeJClient();
                for (final String clientIP : clist) {
                    cl.send(clientIP, new UpdateClientQueueMessage(
                            myIP,
                            clientIP,
                            list
                    ));
                }

            }
        }.start();
    }

    @Override
    public void setList(ArrayList<SongPair> voteList) {
        this.list.clear();
        this.list.addAll(voteList);
    }

    @Override
    public void setServerIP(String serverIP) {
        // Empty
    }
}
