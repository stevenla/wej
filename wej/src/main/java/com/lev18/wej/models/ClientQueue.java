package com.lev18.wej.models;

import com.lev18.wej.Global;
import com.lev18.wej.WeJClient;
import com.lev18.wej.messages.VoteMessage;

import java.util.ArrayList;

/**
 * Created by preston on 2/23/14.
 */
public class ClientQueue implements Queue {

    final private ArrayList<SongPair> voteList;
    private String serverIP;

    public ClientQueue() {
        this.serverIP = "";
        this.voteList = new ArrayList<SongPair>();
    }

    public ClientQueue(String serverIP, ArrayList<SongPair> voteList) {
        this.serverIP = serverIP;
        this.voteList = voteList;
    }

    @Override
    public void vote(String clientID, String trackID, int score) {
        String myIP = Global.getInstance().getMyServerInfo().getIpaddr();

        WeJClient cl = new WeJClient();
        cl.send(this.serverIP, new VoteMessage(
                myIP,
                this.serverIP,
                clientID,
                trackID,
                score
        ));

    }

    @Override
    public ArrayList<SongPair> getList() {
        return this.voteList;
    }

    @Override
    public Song getFront() {
        if (this.voteList.isEmpty()) {
            return null;
        }
        return this.voteList.get(0).songObject;
    }

    @Override
    public void dequeue() {
        // Empty
    }

    @Override
    public void addClient(String clientIP) {
        // Empty
    }

    @Override
    public void setList(ArrayList<SongPair> voteList) {
        this.voteList.clear();
        this.voteList.addAll(voteList);
    }

    public String getServerIP() {
        return serverIP;
    }

    @Override
    public void setServerIP(String serverIP) {
        this.serverIP = serverIP;
    }


}
