package com.lev18.wej.models;
import java.io.Serializable;
import java.util.*;
import android.content.Context;
import android.database.Cursor;
import android.provider.MediaStore;
import android.provider.MediaStore.Audio;


/**
 * Created by Vishnu on 2/22/14.
 */
public class MusicDB implements Serializable {

    // ArrayList, but not List implements Serializable
    private ArrayList<Song> songs;


    public MusicDB (List<Song> database)  {
        songs = new ArrayList<Song> (database);
    }

    public MusicDB() {
        songs = new ArrayList<Song>();
    }

    public void getLocalMusic (Context context) {
        songs.clear();

        String selection = MediaStore.Audio.Media.IS_MUSIC + " != 0";

        String[] projection = {
                Audio.Media.ARTIST,
                Audio.Media.TITLE,
                Audio.Media.ALBUM,
                Audio.Media.DATA,
                MediaStore.Audio.Media._ID,
        };
        Cursor c = context.getContentResolver().query(
                MediaStore.Audio.Media.EXTERNAL_CONTENT_URI, projection, selection, null, null);

        while(c.moveToNext()) {
            Song s = new Song();
            s.artist = c.getString(0);
            s.track = c.getString(1);
            s.album = c.getString(2);
            s.path = c.getString(3);
            s.id = c.getString(4);
            songs.add(s);
        }
    }
    public List<Song> search (String raw_query) {
        String query = raw_query.toLowerCase();
        List<Song> results = new ArrayList<Song>();
        for (Song s : songs) {
            if (s.album.toLowerCase().contains(query) || s.artist.toLowerCase().contains(query)
                    || s.track.toLowerCase().contains(query)) {
                results.add(s);
            }
        }
        return results;
    }

    public Song getSongByID(String id) {
        for (Song s : songs) {
            if (s.id.equals(id)) {
                return s;
            }
        }
        return null;
    }
}
