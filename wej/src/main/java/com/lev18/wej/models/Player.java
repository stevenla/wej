package com.lev18.wej.models;
import android.media.MediaPlayer;
import android.widget.ImageButton;

import com.lev18.wej.Global;
import com.lev18.wej.R;

/**
 * Created by Vishnu on 2/22/14.
 */
public class Player implements MediaPlayer.OnCompletionListener {
    private ImageButton playPauseButton;
    private MediaPlayer mediaPlayer;

    public Player(){
        this.mediaPlayer = new MediaPlayer();
    }

    public void play(Song song) {
        try{
            this.mediaPlayer.setDataSource(song.path);
            this.mediaPlayer.prepare();
            this.mediaPlayer.start();
        } catch (Exception e) {}
        this.mediaPlayer.setOnCompletionListener(this);
    }

    public void stop() {
        this.mediaPlayer.stop();
    }

    public void playPause() {
        if (this.isPlaying())
            this.mediaPlayer.pause();
        else
            this.mediaPlayer.start();
        this.updatePlayPauseButton();
    }

    public void skipTrack() {
        synchronized (this) {
            this.mediaPlayer.reset();
            Global.getInstance().getQueue().dequeue();
            final Song front = Global.getInstance().getQueue().getFront();
            if (front != null)
                play(front);
        }
    }

    public boolean isPlaying() {
        return this.mediaPlayer.isPlaying();
    }

    public void setPlayPauseButton(ImageButton playPause) {
        this.playPauseButton = playPause;
        this.updatePlayPauseButton();
    }

    public void updatePlayPauseButton() {
        if (this.isPlaying()) {
            this.playPauseButton.setImageResource(R.drawable.ic_action_pause);
        }
        else {
            this.playPauseButton.setImageResource(R.drawable.ic_action_play);
        }
    }

    @Override
    public void onCompletion(MediaPlayer mediaPlayer) {
        skipTrack();
        Global.getInstance().updateQueueList();
    }
}
