package com.lev18.wej.models;

import java.io.Serializable;

/**
 * Created by Vishnu on 2/22/14.
 */
public class Song implements Serializable {
    public String artist;
    public String album;
    public String track;
    public String path;
    public String id;

    public Song() {

    }

    public Song(String track, String artist, String album) {
        this.track = track;
        this.artist = artist;
        this.album = album;
    }

}
