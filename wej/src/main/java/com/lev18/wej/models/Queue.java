package com.lev18.wej.models;

import java.util.ArrayList;


/**
 * Created by Jeremy on 2/22/14.
 */
public interface Queue {
    public void vote(String clientID, String trackID, int score);
    public ArrayList<SongPair> getList();
    public Song getFront(); //first song on the list is the current song
    public void dequeue();
    public void addClient(String clientIP);
    public void setList(ArrayList<SongPair> voteList);
    public void setServerIP(String serverIP);
}
