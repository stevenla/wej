package com.lev18.wej.models;

import java.io.Serializable;

/**
 * Created by Vishnu on 2/22/14.
 */
public class ServerInfo implements Serializable {
    private String name = "";
    private String ipaddr = "";

    public ServerInfo () {

    }
    public ServerInfo(String name, String ipaddr) {
        this.name = name;
        this.ipaddr = ipaddr;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getIpaddr() {
        return ipaddr;
    }

    public void setIpaddr(String ipaddr) {
        this.ipaddr = ipaddr;
    }
}
