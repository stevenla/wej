package com.lev18.wej;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.lev18.wej.models.MusicDB;
import com.lev18.wej.models.Song;

import java.util.List;

/**
 * Created by steven on 2/18/14.
 */
public class BrowseFragment extends Fragment {

    BrowseAdapter adapter;
    ListView listView;

    public BrowseFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_browse, container, false);
        this.listView = (ListView) rootView.findViewById(R.id.browseListView);

        this.doSearch("");

        return rootView;
    }

    public void doSearch(String query) {
        // Get the global music database
        MusicDB musicDB = Global.getInstance().musicDB;
        List<Song> songs = musicDB.search(query);

        // Create data adapter for servers
        this.adapter = new BrowseAdapter(getActivity(), R.layout.listview_track, songs);
        this.listView.setAdapter(this.adapter);
    }


}