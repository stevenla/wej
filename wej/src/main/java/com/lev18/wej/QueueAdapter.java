package com.lev18.wej;

import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.TextView;

import com.lev18.wej.models.SongPair;

import java.util.ArrayList;

/**
 * Created by steven on 2/22/14.
 */
public class QueueAdapter extends ArrayAdapter<SongPair> {
    private static class ViewHolder {
        TextView title, subtitle, rating;
        ImageButton playPause;
    }

    private ArrayList<SongPair> songs = null;
    private LayoutInflater inflater;

    public QueueAdapter(Context context, int resource, ArrayList<SongPair> songs) {
        super(context, resource, songs);
        this.songs = songs;
        this.inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent){
        View row = convertView;
        ViewHolder holder;

        final SongPair current = this.songs.get(position);

        if (row == null) {
            row = inflater.inflate(R.layout.listview_track, parent, false);

            // Cache the view fields into a holder
            holder = new ViewHolder();
            holder.title = (TextView) row.findViewById(R.id.trackTitle);
            holder.subtitle = (TextView) row.findViewById(R.id.trackSubtitle);
            holder.rating = (TextView) row.findViewById(R.id.trackRating);
            holder.playPause = (ImageButton) row.findViewById(R.id.playPause);
            row.setTag(holder);
        }
        else {
            // row already exists and should have holder
            holder = (ViewHolder) row.getTag();
        }

        holder.title.setText(current.songObject.track);
        holder.subtitle.setText(current.songObject.artist);
        holder.rating.setText(Integer.toString(current.score));

        // The currently playing track should be special!
        if (position == 0) {
            row.setBackgroundColor(0xffffffff);
            holder.title.setTypeface(null, Typeface.BOLD);
            holder.subtitle.setTypeface(null, Typeface.BOLD);
            holder.rating.setTypeface(null, Typeface.BOLD);
            holder.playPause.setVisibility(ImageButton.VISIBLE);

            // Set global play pause button
            if (Global.getInstance().getType() == Global.Device.SERVER) {
                Global.getInstance().getPlayer().setPlayPauseButton(holder.playPause);

                holder.playPause.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Global.getInstance().getPlayer().playPause();
                        Global.getInstance().updateQueueList();
                    }
                });
            }
            else {
                holder.playPause.setClickable(false);
            }
        }
        // Set background color and stuff back to default because some views may be reused and contain the currently playing style
        else {
            row.setBackgroundColor(0x00000000);
            holder.title.setTypeface(null, Typeface.NORMAL);
            holder.subtitle.setTypeface(null, Typeface.NORMAL);
            holder.rating.setTypeface(null, Typeface.NORMAL);
            holder.playPause.setVisibility(ImageButton.GONE);
        }

        // Bind events

        ImageButton downvote = (ImageButton) row.findViewById(R.id.downvote);
        downvote.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Global.getInstance().getQueue().vote(Global.getInstance().getClientID(), current.songObject.id, -1);
                Global.getInstance().updateQueueList();
            }
        });

        ImageButton upvote = (ImageButton) row.findViewById(R.id.upvote);
        upvote.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Global.getInstance().getQueue().vote(Global.getInstance().getClientID(), current.songObject.id, 1);
                Global.getInstance().updateQueueList();
            }
        });

        return row;
    }

}
