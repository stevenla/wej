package com.lev18.wej;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.lev18.wej.models.Song;

import java.util.List;

/**
 * Created by steven on 2/22/14.
 */
public class BrowseAdapter extends ArrayAdapter<Song> {
    private List<Song> songs = null;
    private LayoutInflater inflater;
    private Context context;

    public BrowseAdapter(Context context, int resource, List<Song> songs) {
        super(context, resource, songs);
        this.context = context;
        this.songs = songs;
        this.inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent){
        View row = convertView;
        final Song current = this.songs.get(position);

        if (row == null) {
            row = inflater.inflate(R.layout.listview_track, parent, false);
        }

        // Get all the layout views
        TextView title = (TextView) row.findViewById(R.id.trackTitle);
        TextView subtitle = (TextView) row.findViewById(R.id.trackSubtitle);
        LinearLayout controls = (LinearLayout) row.findViewById(R.id.trackControls);

        // Set the layout text and visibilities
        controls.setVisibility(LinearLayout.GONE);
        title.setText(current.track);
        subtitle.setText(current.artist);

        row.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // Vote on the song
                Global.getInstance().getQueue().vote(Global.getInstance().getClientID(), current.id, 1);

                // Show a toast
                final CharSequence text = String.format("Added %s by %s", current.track, current.artist);
                final int duration = Toast.LENGTH_SHORT;
                Toast toast = Toast.makeText(context, text, duration);
                toast.show();
                Global.getInstance().updateQueueList();
            }
        });

        return row;
    }

}
