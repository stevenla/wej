package com.lev18.wej;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

/**
 * Created by steven on 2/18/14.
 */
public class QueueFragment extends Fragment {

    public QueueFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_queue, container, false);

        // Set listView that will get updated globally lol
        ListView listView = (ListView) rootView.findViewById(R.id.queueListView);

        Global.getInstance().queueListAdapter = new QueueAdapter(getActivity(), R.layout.listview_track, Global.getInstance().getQueueListBuffer());
        listView.setAdapter(Global.getInstance().queueListAdapter);

        // Update the queue list with what's in the db
        Global.getInstance().updateQueueList();

        return rootView;
    }

    @Override
    public void onResume() {
        Global.getInstance().updateQueueList();
        super.onResume();
    }

}