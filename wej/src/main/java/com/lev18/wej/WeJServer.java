package com.lev18.wej;

import com.lev18.wej.messages.WeJMessage;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.net.ServerSocket;
import java.net.Socket;

/**
 * Created by Vishnu on 2/22/14.
 */
public class WeJServer{

    private ServerSocket serverSocket;

    Thread serverThread = null;

    public static final int SERVERPORT = 6000;

    public WeJServer() {
        this.serverThread = new Thread(new ServerThread());
        this.serverThread.start();
    }

    public void stopSocket() {
        try {
            serverSocket.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    class ServerThread implements Runnable {

        public void run() {
            Socket socket = null;
            try {
                serverSocket = new ServerSocket(SERVERPORT);
            } catch (IOException e) {
                e.printStackTrace();
            }
            while (!Thread.currentThread().isInterrupted()) {

                try {

                    socket = serverSocket.accept();

                    CommunicationThread commThread = new CommunicationThread(socket);
                    new Thread(commThread).start();

                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    class CommunicationThread implements Runnable {

        private Socket clientSocket;

        private ObjectInputStream inputStream;
        private WeJMessage message;

        public CommunicationThread(Socket clientSocket) {

            this.clientSocket = clientSocket;

            try {
                inputStream = new ObjectInputStream(this.clientSocket.getInputStream());
                message = (WeJMessage) inputStream.readObject();
                message.onReceive();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            }
        }

        public void run() {

        }
    }

}
