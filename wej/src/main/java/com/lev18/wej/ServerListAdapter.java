package com.lev18.wej;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.lev18.wej.models.ServerInfo;

import java.util.ArrayList;

/**
 * Created by steven on 2/22/14.
 */
public class ServerListAdapter extends ArrayAdapter<ServerInfo> {
    private ArrayList<ServerInfo> data = null;
    private LayoutInflater inflater;
    private Context context;
    private int resource;

    public ServerListAdapter(Context context, int resource, ArrayList<ServerInfo> data) {
        super(context, resource, data);
        this.data = data;
        this.context = context;
        this.resource = resource;
        this.inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent){
        View row = convertView;
        final ServerInfo current = this.data.get(position);

        if (row == null) {
            row = inflater.inflate(this.resource, parent, false);
        }

        TextView text1 = (TextView) row.findViewById(android.R.id.text1);
        TextView text2 = (TextView) row.findViewById(android.R.id.text2);

        text1.setText(this.data.get(position).getName());
        text2.setText(this.data.get(position).getIpaddr());


        return row;
    }

}
