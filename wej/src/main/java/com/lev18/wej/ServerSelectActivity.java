package com.lev18.wej;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.text.InputType;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;
import android.net.wifi.WifiManager;
import android.net.wifi.WifiInfo;

import com.lev18.wej.messages.WJMServerQuery;
import com.lev18.wej.models.MusicDB;
import com.lev18.wej.models.ServerInfo;

import java.util.ArrayList;

public class ServerSelectActivity extends WeJActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_server_select);

        Global.getInstance().init(this);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.server_select, menu);
        return true;
    }

    private String getMyIpAddress() {
        WifiManager wifiManager = (WifiManager) getSystemService(WIFI_SERVICE);
        WifiInfo wifiInfo = wifiManager.getConnectionInfo();
        int ip = wifiInfo.getIpAddress();

        String ipString = String.format(
                "%d.%d.%d.%d",
                (ip & 0xff),
                (ip >> 8 & 0xff),
                (ip >> 16 & 0xff),
                (ip >> 24 & 0xff));
        return ipString;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        switch (id) {
            case R.id.action_new_server:
                newServerDialog();
                break;
            case R.id.action_refresh_server:
                String myIpAddress = getMyIpAddress();
                Global.getInstance().setMyServerInfo("CLIENT-" + Global.getInstance().getClientID(), myIpAddress);
                int me = Integer.parseInt(myIpAddress.substring(myIpAddress.lastIndexOf('.')+1));
                String subnet = myIpAddress.substring(0,myIpAddress.lastIndexOf('.')+1);
                Global.getInstance().serverList = new ArrayList<ServerInfo>();
                for(int i = 1; i < 254; i++) {
                    if (i == me) continue;
                    WeJClient wejc = new WeJClient();
                    WJMServerQuery q = new WJMServerQuery(myIpAddress, subnet+i, "");
                    wejc.send(subnet+i, q);
                }

                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private void newServerDialog() {
        final Activity context = this;

        // Create the Server Name input in the dialog
        final EditText input = new EditText(this);
        input.setInputType(InputType.TYPE_CLASS_TEXT);
        input.setHint("Server Name");

        // 1. Instantiate an AlertDialog.Builder with its constructor
        AlertDialog.Builder builder = new AlertDialog.Builder(this);

        // 2. Chain together various setter methods to set the dialog characteristics
        builder.setTitle(R.string.action_server_new)
                .setMessage(R.string.server_new_message)
                .setView(input)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {

                        // Grab data from input
                        final String serverName =
                                (input.getText() != null && input.getText().length() > 0)
                                        ? input.getText().toString()
                                        : "WeJ";

                        // populate server info
                        Global.getInstance().setMyServerInfo(serverName, getMyIpAddress());


                        // Start Intent to go to Queue activity
                        Intent intent = new Intent(context.getBaseContext(), QueueActivity.class);

                        // Create server
                        Global.getInstance().setType(Global.Device.SERVER);
                        MusicDB musicDB = Global.getInstance().musicDB;
                        musicDB.getLocalMusic(context);

                        startActivity(intent);

                    }
                })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        // User cancelled the dialog
                        Log.d("WeJ", "NVM");
                    }
                });
            ;

        // 3. Get the AlertDialog from create()
        AlertDialog dialog = builder.create();

        dialog.show();

    }

}
