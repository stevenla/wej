package com.lev18.wej;

import com.lev18.wej.messages.WeJMessage;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.net.InetAddress;
import java.net.Socket;
import java.net.UnknownHostException;

/**
 * Created by Vishnu on 2/22/14.
 */
public class WeJClient {

    private static final int SERVERPORT = 6000;

    public WeJClient() {

    }

    public void send(String serverIP, WeJMessage message) {
        try {
            new Thread(new ClientThread(serverIP, message)).start();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    class ClientThread implements Runnable {
        private String serverIP;
        private WeJMessage message;

        public ClientThread(String serverIP, WeJMessage message) {
            this.serverIP = serverIP;
            this.message = message;
        }

        @Override
        public void run() {

            try {
                InetAddress serverAddr = InetAddress.getByName(this.serverIP);

                Socket socket = new Socket(serverAddr, SERVERPORT);

                ObjectOutputStream outputStream = new ObjectOutputStream(socket.getOutputStream());
                outputStream.writeObject(this.message);
                outputStream.flush();

            } catch (UnknownHostException e1) {
                e1.printStackTrace();
            } catch (IOException e1) {
                e1.printStackTrace();
            } catch (Exception e) {
                e.printStackTrace();
            }

        }

    }
}
