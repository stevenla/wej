package com.lev18.wej.messages;

import com.lev18.wej.models.ClientQueue;
import com.lev18.wej.Global;
import com.lev18.wej.models.SongPair;

import java.util.ArrayList;

/**
* Created by preston on 2/24/14.
*/
public class UpdateClientQueueMessage extends WeJMessage {
    final private ArrayList<SongPair> voteList;

    public UpdateClientQueueMessage(String senderIP, String receiverIP, ArrayList<SongPair> clientVoteList) {
        super(senderIP, receiverIP);
        this.voteList = clientVoteList;
    }

    @Override
    public void onReceive() {
        Global.getInstance().getQueue().setList(this.voteList);
        Global.getInstance().updateQueueList();
    }
}
