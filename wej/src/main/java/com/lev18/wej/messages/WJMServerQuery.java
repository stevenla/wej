package com.lev18.wej.messages;

import com.lev18.wej.Global;
import com.lev18.wej.models.ServerInfo;
import com.lev18.wej.WeJClient;

/**
 * Created by Vishnu on 2/22/14.
 */
public class WJMServerQuery extends WeJMessage {

    public String data = "";
    public String response = "";
    public ServerInfo serverInfo = null;

    public WJMServerQuery(String senderIP, String receiverIP, String data ) {
        super(senderIP, receiverIP);
        this.data = data;
    }

    @Override
    public void onReceive() {
        if(Global.getInstance().getType() == Global.Device.SERVER) {
            // Server Receiving
            serverInfo = Global.getInstance().getMyServerInfo();

            WeJClient cl = new WeJClient();
            WJMServerQuery q = new WJMServerQuery(receiverIP,senderIP,"I AM A SERVER");
            q.serverInfo = serverInfo;
            cl.send(this.senderIP, q);
        }
        else if (this.data.equals("I AM A SERVER")) {
            // Client receiving
            Global.getInstance().addToServerList(this.serverInfo);
            Global.getInstance().serverListView.post(new Runnable() {
                @Override
                public void run() {

                    Global.getInstance().updateServerListView();
                    Global.getInstance().serverListView.setAdapter(Global.getInstance().serverListAdapter);
                }

            });
        }

    }


}
