package com.lev18.wej.messages;

import com.lev18.wej.models.ClientQueue;
import com.lev18.wej.Global;
import com.lev18.wej.models.MusicDB;
import com.lev18.wej.models.SongPair;

import java.util.ArrayList;

/**
* Created by preston on 2/24/14.
*/
public class InitializeClientQueueMessage extends WeJMessage {
    private MusicDB musicDB;
    private ArrayList<SongPair> voteList;

    public InitializeClientQueueMessage(String senderIP, String receiverIP, MusicDB musicDB, ArrayList<SongPair> voteList) {
        super(senderIP, receiverIP);
        this.musicDB = musicDB;
        this.voteList = voteList;
    }

    @Override
    public void onReceive() {
        Global.getInstance().musicDB = musicDB;
        Global.getInstance().getQueue().setServerIP(this.senderIP);
        Global.getInstance().getQueue().setList(this.voteList);
        Global.getInstance().updateQueueList();
    }
}
