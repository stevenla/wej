package com.lev18.wej.messages;

import com.lev18.wej.Global;

/**
* Created by preston on 2/24/14.
*/
public class SubscribeMessage extends WeJMessage {

    /**
     * @param senderIP   The source IP address
     * @param receiverIP The destination IP address
     */
    public SubscribeMessage(String senderIP, String receiverIP) {
        super(senderIP, receiverIP);
    }

    @Override
    public void onReceive() {
        Global.getInstance().getQueue().addClient(this.senderIP);
    }
}
