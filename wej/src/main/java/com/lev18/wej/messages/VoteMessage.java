package com.lev18.wej.messages;

import com.lev18.wej.Global;

/**
* Created by preston on 2/24/14.
*/
public class VoteMessage extends WeJMessage {
    final private String clientID;
    final private String trackID;
    final private int score;

    public VoteMessage(String senderIP, String receiverIP, String clientID, String trackID, int score) {
        super(senderIP, receiverIP);
        this.clientID = clientID;
        this.trackID = trackID;
        this.score = score;
    }

    @Override
    public void onReceive() {
        Global.getInstance().getQueue().vote(this.clientID, this.trackID, this.score);
    }
}
