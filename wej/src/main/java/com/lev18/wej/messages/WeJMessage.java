package com.lev18.wej.messages;

import java.io.Serializable;

/**
 * Created by Vishnu on 2/22/14.
 */
public abstract class WeJMessage implements Serializable {
    final public String senderIP;
    final public String receiverIP;

    /**
     *
     * @param senderIP The source IP address
     * @param receiverIP The destination IP address
     */
    public WeJMessage(String senderIP, String receiverIP) {
        this.senderIP = senderIP;
        this.receiverIP = receiverIP;
    }

    public abstract void onReceive();
}
