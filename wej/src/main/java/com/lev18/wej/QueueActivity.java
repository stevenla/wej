package com.lev18.wej;

import android.app.Activity;
import android.app.ActionBar;
import android.app.AlertDialog;
import android.app.Fragment;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.os.Build;
import android.widget.Toast;

public class QueueActivity extends WeJActivity {

    private String[] serverInfo = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_queue);

        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            this.serverInfo = extras.getStringArray("SERVER_INFO");
        }

        ActionBar actionBar = getActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        if (this.serverInfo != null)
            actionBar.setSubtitle(this.serverInfo[0]);
    }

    private void showCloseConfirmation() {
        final Activity activity = this;
        new AlertDialog.Builder(this)
                .setMessage("Are you sure you want to close your server?")
                .setCancelable(true)
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        // Clean up the server
                        Global.getInstance().getPlayer().stop();
                        activity.finish();
                    }
                })
                .setNegativeButton("No", null)
                .show();
    }

    @Override
    public void onBackPressed() {
        // It's OK to press back on a client
        if (Global.getInstance().getType() == Global.Device.CLIENT) {
            super.onBackPressed();
            return;
        }

        showCloseConfirmation();
    }

    @Override
    public boolean onNavigateUp() {
        // It's OK to press back on a client
        if (Global.getInstance().getType() == Global.Device.CLIENT) {
            return super.onNavigateUp();
        }

        showCloseConfirmation();
        return false;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.queue, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_add_track) {
            Intent intent = new Intent(this.getBaseContext(), BrowseActivity.class);
            startActivity(intent);
        }
        return super.onOptionsItemSelected(item);
    }

}
