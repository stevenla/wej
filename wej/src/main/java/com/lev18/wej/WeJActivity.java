package com.lev18.wej;

import android.app.Activity;

/**
 * Created by steven on 2/24/14.
 */
public class WeJActivity extends Activity {

    @Override
    protected void onResume() {
        super.onResume();
        Global.getInstance().setCurrentActivity(this);
    }

    @Override
    protected void onPause() {
        Global.getInstance().setCurrentActivity(null);
        super.onPause();
    }

}
