package com.lev18.wej;

import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import com.lev18.wej.messages.SubscribeMessage;
import com.lev18.wej.models.ServerInfo;

import java.util.ArrayList;

/**
 * Created by steven on 2/18/14.
 */
public class ServerSelectFragment extends Fragment {

    public ServerSelectFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_server_select, container, false);
        Global.getInstance().serverListView = (ListView) rootView.findViewById(R.id.serverListView);

        final ArrayList<ServerInfo> list = Global.getInstance().getServerList();
        Global.getInstance().serverListContext = getActivity();

        // Create data adapter for servers
        Global.getInstance().serverListAdapter = new ServerListAdapter(getActivity(), android.R.layout.simple_list_item_2, list);
        Global.getInstance().serverListView.setAdapter(Global.getInstance().serverListAdapter);

        // When clicking on an item, take them to that server's queue
        Global.getInstance().serverListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                // Set device to client
                Global.getInstance().setType(Global.Device.CLIENT);

                // Request info from server
                final String myIP = Global.getInstance().getMyServerInfo().getIpaddr();
                final String serverIP = Global.getInstance().getServerList().get(position).getIpaddr();
                WeJClient cl = new WeJClient();
                cl.send(serverIP, new SubscribeMessage(myIP, serverIP));

                Intent intent = new Intent(getActivity().getBaseContext(), QueueActivity.class);
                startActivity(intent);
            }
        });


        return rootView;

    }

}