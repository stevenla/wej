package com.lev18.wej;


import android.app.Activity;
import android.content.Context;
import android.provider.Settings;
import android.widget.ListView;

import com.lev18.wej.models.ClientQueue;
import com.lev18.wej.models.MusicDB;
import com.lev18.wej.models.Player;
import com.lev18.wej.models.Queue;
import com.lev18.wej.models.ServerInfo;
import com.lev18.wej.models.ServerQueue;
import com.lev18.wej.models.SongPair;

import java.util.ArrayList;

/**
 * Created by steven on 2/22/14.
 * Usage:
 * Global.getInstance().device = Global.Device.SERVER;
 */
public class Global {
    public ListView serverListView;
    public Context serverListContext;

    public void updateServerListView() {
        this.serverListAdapter = new ServerListAdapter(serverListContext, android.R.layout.simple_list_item_2, Global.getInstance().getServerList());
        this.serverListView.setAdapter(this.serverListAdapter);
    }

    // Static stuff for singletons
    public static enum Device {
        CLIENT, SERVER
    }

    private static Global instance;

    static {
        instance = new Global();
    }

    /**
     * Returns the singleton instance of the class
     *
     * @return The singleton instance of the class
     */
    public static Global getInstance() {
        return instance;
    }

    private Device type = Device.CLIENT;
    private Queue queue = new ServerQueue();
    private ArrayList<SongPair> queueListBuffer = new ArrayList<SongPair>();
    private String clientID = "";

    public QueueAdapter queueListAdapter;
    public MusicDB musicDB = new MusicDB();
    private Player player = new Player();

    public Player getPlayer() {
        return player;
    }

    public WeJServer wejs = new WeJServer();

    public ArrayList<ServerInfo> serverList = new ArrayList<ServerInfo>();

    public ArrayList<ServerInfo> getServerList() {
        return serverList;
    }
    public void addToServerList(ServerInfo si) {
        serverList.add(si);
    }

    public int getServerListSize() {
        return serverList.size();
    }

    public ServerListAdapter serverListAdapter;


    public ServerInfo getMyServerInfo() {
        return myServerInfo;
    }

    public void setMyServerInfo(String name, String ipaddr) {
        this.myServerInfo.setName(name);
        this.myServerInfo.setIpaddr(ipaddr);
    }

    public ServerInfo myServerInfo = new ServerInfo();

    public void setType(Device type) {
        this.type = type;
        if (this.type == Device.SERVER) {
            this.queue = new ServerQueue();
        }
        else {
            this.queue = new ClientQueue();
        }
    }

    public Device getType() {
        return this.type;
    }

    public WeJServer getWEJS() {
        return this.wejs;
    }

    public void init(Context context) {
        this.clientID = Settings.Secure.getString(context.getContentResolver(),
                Settings.Secure.ANDROID_ID);
    }

    public String getClientID() {
        return this.clientID;
    }

    public Queue getQueue() {
        return this.queue;
    }

    public void setQueue(Queue queue) {
        this.queue = queue;
    }

    public ArrayList<SongPair> getQueueListBuffer() {
        return this.queueListBuffer;
    }

    public void updateQueueList() {
        // If there's not current activity, there's nothing to update
        if (getCurrentActivity() == null)
            return;

        // Otherwise update the UI on the UI thread
        getCurrentActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (queueListAdapter != null) {
                    getQueueListBuffer().clear();
                    getQueueListBuffer().addAll(getQueue().getList());
                    queueListAdapter.notifyDataSetChanged();
                }
            }
        });
    }

    // For getting the current activity to be passed in as a context
    private Activity currentActivity;

    public Activity getCurrentActivity() {
        return this.currentActivity;
    }

    public void setCurrentActivity(Activity activity) {
        this.currentActivity = activity;
    }

}
